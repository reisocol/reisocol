# Hello! 👋

## 📝 Website

- Personal website and blog: https://reiso-portfolio.webflow.io/works

## 🔧 Technologies & Tools
- C++ | C# | Java | Python
- Visual Studio 2019/2022
- Rider
- Unreal (Blueprints/ C++)
- Unity
- DirectX 9, DirectX 11

## 🗂️ Highlight Projects

#### Volleyball Court Test Bed Physics Application (C++/DirectX 9/PhysX)
<a href="https://github.com/reisro/rainforest/tree/master"> <b>Github link</b>
</a><br>

#### Asai Maki TP Game (Unreal 4/Blueprints/C++)
<a href="https://github.com/reisro/asaimaki_tpgame/tree/develop"> <b>Github link</b>
</a><br>

#### Air Hockey C++/DirectX 9
<a href="https://gitlab.com/reisocol/airhockey_cppdx"> <b>Under Development</b>
</a><br>

#### Deferred Rendering C++/DirectX 11
<a href="https://gitlab.com/reisocol/graphicsapplication"> <b>Under Development</b>
</a><br>

<!-- ## 👨‍💻 This week, I spent my time on:

[![zhenye's wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=nazhenye&line_height=27&title_color=6aa6f8&text_color=8a919a&icon_color=6aa6f8&bg_color=22272e)](https://github.com/anuraghazra/github-readme-stats) -->
